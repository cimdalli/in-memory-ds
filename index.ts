import * as Datastore from '@google-cloud/datastore'
import { DatastoreKey } from '@google-cloud/datastore/entity'

type DatastoreData = {}

interface MutationReq {
  method: 'commit'
  reqOpts: {
    mutations: (
      | {
          upsert: {
            key: any
            properties: any
          }
        }
      | {
          delete: {
            key: any
          }
        })[]
  }
}

interface LookupReq {
  method: 'lookup'
  reqOpts: { keys: any[] }
}

interface QueryReq {
  method: 'runQuery'
  reqOpts: {
    query: any
  }
}

class InMemoryDatastore extends Datastore {
  constructor(private data: DatastoreData = {}) {
    super({})
  }

  keyToString = (key: { path: DatastoreKey[] }) => {
    return `${key.path[0].kind}.${key.path[0].name || key.path[0].id}`
  }

  // tslint:disable-next-line:variable-name
  request_ = (
    options: LookupReq | MutationReq | QueryReq,
    callback: (err: Error | undefined, res?: any) => void,
  ) => {
    // delete, upsert
    if (options.method === 'commit') {
      const { mutations } = options.reqOpts

      mutations.forEach(m => {
        if (m['upsert']) {
          const { key, properties } = m['upsert']
          this.data[this.keyToString(key)] = properties
        }
        if (m['delete']) {
          const { key } = m['delete']
          delete this.data[key]
        }
      })

      return callback(undefined)
    }

    // createReadStream
    if (options.method === 'lookup') {
      const { keys } = options.reqOpts
      const found = keys
        .map(key => {
          const keyStr = this.keyToString(key)
          if (this.data[keyStr]) {
            return {
              entity: {
                key: {
                  path: key.path.map((k: any) => ({
                    ...k,
                    idType: k.id ? 'id' : 'name',
                  })),
                },
                properties: Object.entries(this.data[keyStr]).reduce(
                  (prev, [key, proto]) => {
                    const [valueType, values] = Object.entries(proto)[0]
                    return {
                      ...prev,
                      [key]: { ...proto, valueType, values },
                    }
                  },
                  {},
                ),
              },
            }
          }
        })
        .filter(x => x)

      return callback(undefined, {
        found,
      })
    }

    // query
    if (options.method === 'runQuery') {
      const { query } = options.reqOpts

      return callback(undefined)
    }
  }
}
